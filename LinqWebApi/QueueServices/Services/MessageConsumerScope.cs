﻿using System;
using System.Collections.Generic;
using System.Text;
using RabbitMQ.Client;
using QueueServices.Interfaces;
using QueueServices.Models;

namespace QueueServices.Services
{
    public class MessageConsumerScope// : IMessageConsumerScope, IDisposable
    {
        private readonly MessageScopeSettings messageScopeSettings;
        private readonly Lazy</*IMessageQueue*/MessageQueue> messageQueueLazy;
        private readonly Lazy</*IMessageConsumer*/MessageConsumer> messageConsumerLazy;
        private readonly IConnectionFactory connectionFactory;

        public /*IMessageConsumer*/MessageConsumer MessageConsumer => messageConsumerLazy.Value;
        public /*IMessageQueue*/MessageQueue MessageQueue => messageQueueLazy.Value;

        public MessageConsumerScope(IConnectionFactory connFactory, MessageScopeSettings messageScopeSett)
        {
            connectionFactory = connFactory;
            messageScopeSettings = messageScopeSett;
            messageQueueLazy = new Lazy</*IMessageQueue*/MessageQueue>(CreateMessageQueue);
            messageConsumerLazy = new Lazy</*IMessageConsumer*/MessageConsumer>(CreateMessageConsumer);
        }

        private /*IMessageQueue*/MessageQueue CreateMessageQueue()
        {
            return new MessageQueue(connectionFactory, messageScopeSettings);
        }

        private /*IMessageConsumer*/MessageConsumer CreateMessageConsumer()
        {
            return new MessageConsumer(new MessageConsumerSettings
            {
                Channel = MessageQueue.Channel,
                QueueName = messageScopeSettings.QueueName
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using QueueServices.Interfaces;
using QueueServices.Models;
using RabbitMQ.Client;

namespace QueueServices.Services
{
    public class MessageConsumerScopeFactory// : IMessageConsumerScopeFactory
    {
        private readonly /*IConnectionFactory*/ConnectionFactory connectionFactory;
        
        public MessageConsumerScopeFactory(/*IConnectionFactory*/ConnectionFactory connFactory)
        {
            connectionFactory = connFactory;
        }

        public /*IMessageConsumerScope*/MessageConsumerScope Open(MessageScopeSettings messageScopeSettings)
        {
            return new MessageConsumerScope(connectionFactory, messageScopeSettings);
        }

        public /*IMessageConsumerScope*/MessageConsumerScope Connect(MessageScopeSettings messageScopeSettings)
        {
            var mqConsumerScope = Open(messageScopeSettings);
            mqConsumerScope.MessageConsumer.Connect();

            return mqConsumerScope;
        }
    }
}

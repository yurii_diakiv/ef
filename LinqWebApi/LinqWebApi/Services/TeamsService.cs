﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using LinqWebApi.Interfaces;
//using LinqWebApi.Models;
using DataAccess.Entities;
using Newtonsoft.Json;
using LinqWebApi.Repositories;

namespace LinqWebApi.Services
{
    public class TeamsService : ITeamsService
    {
        private readonly IRepository<Team> repository;

        public TeamsService(TeamRepository teamRepository)
        {
            repository = teamRepository;
        }
        public IEnumerable<Team> GetTeams()
        {
            return repository.GetItems();
        }

        public Team GetTeam(int id)
        {
            return repository.GetItem(id);
        }

        public void Create(Team item)
        {
            repository.Create(item);
        }

        public void Update(Team team)
        {
            repository.Update(team);
        }

        public void Delete(int id)
        {
            repository.Delete(id);
        }
    }



    //IRepository<Team> repository = new TeamRepository();
    //public List<Team> GetTeams()
    //{
    //    return repository.GetItemList();
    //}

    //public Team GetTeam(int id)
    //{
    //    return repository.GetItem(id);
    //}

    //public void Create(Team item)
    //{
    //    repository.Create(item);
    //}

    //public void Update(int id, Team team)
    //{
    //    repository.Update(id, team);
    //}

    //public void Delete(int id)
    //{
    //    repository.Delete(id);
    //}
//}
}

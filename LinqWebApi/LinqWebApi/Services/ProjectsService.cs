﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using System.Net.Http;
using LinqWebApi.Interfaces;
//using LinqWebApi.Models;
using DataAccess.Entities;
using LinqWebApi.Repositories;
//using Newtonsoft.Json;

namespace LinqWebApi.Services
{
    public class ProjectsService : IProjectsService
    {
        private readonly IRepository<Project> repository;

        public ProjectsService(ProjectRepository projectRepository)
        {
            repository = projectRepository;
        }
        public IEnumerable<Project> GetProjects()
        {
            return repository.GetItems();
        }

        public Project GetProject(int id)
        {
            return repository.GetItem(id);
        }

        public void Create(Project item)
        {
            repository.Create(item);
        }

        public void Update(Project project)
        {
            repository.Update(project);
        }

        public void Delete(int id)
        {
            repository.Delete(id);
        }
    }
}

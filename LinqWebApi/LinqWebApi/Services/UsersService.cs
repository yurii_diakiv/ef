﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using LinqWebApi.Interfaces;
//using LinqWebApi.Models;
using DataAccess.Entities;
using Newtonsoft.Json;
using LinqWebApi.Repositories;

namespace LinqWebApi.Services
{
    public class UsersService : IUsersService
    {
        private readonly IRepository<User> repository;

        public UsersService(UserRepository userRepository)
        {
            repository = userRepository;
        }
        public IEnumerable<User> GetUsers()
        {
            return repository.GetItems();
        }

        public User GetUser(int id)
        {
            return repository.GetItem(id);
        }

        public void Create(User item)
        {
            repository.Create(item);
        }

        public void Update(User user)
        {
            repository.Update(user);
        }

        public void Delete(int id)
        {
            repository.Delete(id);
        }



        //IRepository<User> repository = new UserRepository();
        //public List<User> GetUsers()
        //{
        //    return repository.GetItemList();
        //}

        //public User GetUser(int id)
        //{
        //    return repository.GetItem(id);
        //}

        //public void Create(User item)
        //{
        //    repository.Create(item);
        //}

        //public void Update(int id, User user)
        //{
        //    repository.Update(id, user);
        //}

        //public void Delete(int id)
        //{
        //    repository.Delete(id);
        //}
    }
}

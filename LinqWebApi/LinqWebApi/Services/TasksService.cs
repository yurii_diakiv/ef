﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using LinqWebApi.Interfaces;
//using LinqWebApi.Models;
using Newtonsoft.Json;
using LinqWebApi.Repositories;

namespace LinqWebApi.Services
{
    public class TasksService : ITasksService
    {
        private readonly IRepository<DataAccess.Entities.Task> repository;

        public TasksService(TaskRepository taskRepository)
        {
            repository = taskRepository;
        }
        public IEnumerable<DataAccess.Entities.Task> GetTasks()
        {
            return repository.GetItems();
        }

        public DataAccess.Entities.Task GetTask(int id)
        {
            return repository.GetItem(id);
        }

        public void Create(DataAccess.Entities.Task item)
        {
            repository.Create(item);
        }

        public void Update(DataAccess.Entities.Task task)
        {
            repository.Update(task);
        }

        public void Delete(int id)
        {
            repository.Delete(id);
        }


        //IRepository<Models.Task> repository = new TaskRepository();
        //public List<Models.Task> GetTasks()
        //{
        //    return repository.GetItemList();
        //}

        //public Models.Task GetTask(int id)
        //{
        //    return repository.GetItem(id);
        //}

        //public void Create(Models.Task item)
        //{
        //    repository.Create(item);
        //}

        //public void Update(int id, Models.Task task)
        //{
        //    repository.Update(id, task);
        //}

        //public void Delete(int id)
        //{
        //    repository.Delete(id);
        //}
    }
}

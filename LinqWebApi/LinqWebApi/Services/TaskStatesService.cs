﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using LinqWebApi.Interfaces;
//using LinqWebApi.Models;
using DataAccess.Entities;
using Newtonsoft.Json;
using LinqWebApi.Repositories;

namespace LinqWebApi.Services
{
    public class TaskStatesService : ITaskStatesService
    {
        private readonly IRepository<TaskState> repository;

        public TaskStatesService(TaskStateRepository taskStateRepository)
        {
            repository = taskStateRepository;
        }
        public IEnumerable<TaskState> GetTaskStates()
        {
            return repository.GetItems();
        }

        public TaskState GetTaskState(int id)
        {
            return repository.GetItem(id);
        }

        public void Create(TaskState item)
        {
            repository.Create(item);
        }

        public void Update(TaskState taskState)
        {
            repository.Update(taskState);
        }

        public void Delete(int id)
        {
            repository.Delete(id);
        }



        //IRepository<TaskState> repository = new TaskStateRepository();
        //public List<TaskState> GetTaskStates()
        //{
        //    return repository.GetItemList();
        //}

        //public TaskState GetTaskState(int id)
        //{
        //    return repository.GetItem(id);
        //}

        //public void Create(TaskState item)
        //{
        //    repository.Create(item);
        //}

        //public void Update(int id, TaskState taskState)
        //{
        //    repository.Update(id, taskState);
        //}

        //public void Delete(int id)
        //{
        //    repository.Delete(id);
        //}
    }
}

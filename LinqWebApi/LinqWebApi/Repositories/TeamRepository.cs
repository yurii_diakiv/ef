﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using LinqWebApi.Models;
using DataAccess.Entities;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace LinqWebApi.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly ProjectDbContext dbContext;

        public TeamRepository(ProjectDbContext projectDbContext)
        {
            dbContext = projectDbContext;
        }

        public IEnumerable<Team> GetItems()
        {
            return dbContext.Set<Team>().AsEnumerable();
        }

        public Team GetItem(int id)
        {
            return dbContext.Set<Team>().Find(id);
        }

        public void Create(Team p)
        {
            dbContext.Set<Team>().Add(p);
            dbContext.SaveChanges();
        }

        public void Update(Team p)
        {
            dbContext.Entry(p).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var p = dbContext.Set<Team>().Find(id);
            dbContext.Set<Team>().Remove(p);
            dbContext.SaveChanges();
        }


        //public LinqWebApi.Data data = new LinqWebApi.Data();
        //List<Team> teams = new List<Team>();

        //public TeamRepository()
        //{
        //    teams = data.GetTeams().ToList();
        //}
        //public List<Team> GetItemList()
        //{
        //    return teams;
        //}

        //public Team GetItem(int id)
        //{
        //    return teams.Find(t => t.Id == id);
        //}

        //public void Create(Team t)
        //{
        //    teams.Add(t);
        //}

        //public void Update(int id, Team t)
        //{
        //    int i = teams.FindIndex(te => te.Id == id);
        //    teams[i] = t;
        //}

        //public void Delete(int id)
        //{
        //    Team t = teams.Find(te => te.Id == id);
        //    if (t != null)
        //    {
        //        teams.Remove(t);
        //    }
        //}
    }
}

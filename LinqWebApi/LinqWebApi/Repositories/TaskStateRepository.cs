﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using LinqWebApi.Models;
using DataAccess.Entities;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace LinqWebApi.Repositories
{
    public class TaskStateRepository : IRepository<TaskState>
    {
        private readonly ProjectDbContext dbContext;

        public TaskStateRepository(ProjectDbContext projectDbContext)
        {
            dbContext = projectDbContext;
        }

        public IEnumerable<TaskState> GetItems()
        {
            return dbContext.Set<TaskState>().AsEnumerable();
        }

        public TaskState GetItem(int id)
        {
            return dbContext.Set<TaskState>().Find(id);
        }

        public void Create(TaskState p)
        {
            dbContext.Set<TaskState>().Add(p);
            dbContext.SaveChanges();
        }

        public void Update(TaskState p)
        {
            dbContext.Entry(p).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var p = dbContext.Set<TaskState>().Find(id);
            dbContext.Set<TaskState>().Remove(p);
            dbContext.SaveChanges();
        }


        //public LinqWebApi.Data data = new LinqWebApi.Data();
        //List<TaskState> taskStates;

        //public TaskStateRepository()
        //{
        //    taskStates = data.GetTaskStates().ToList();
        //}
        //public List<TaskState> GetItemList()
        //{
        //    return taskStates;
        //}

        //public TaskState GetItem(int id)
        //{
        //    return taskStates.Find(ts => ts.Id == id);
        //}

        //public void Create(TaskState ts)
        //{
        //    taskStates.Add(ts);
        //}

        //public void Update(int id, TaskState t)
        //{
        //    int i = taskStates.FindIndex(ts => ts.Id == id);
        //    taskStates[i] = t;
        //}

        //public void Delete(int id)
        //{
        //    TaskState t = taskStates.Find(ts => ts.Id == id);
        //    if (t != null)
        //    {
        //        taskStates.Remove(t);
        //    }
        //}
    }
}

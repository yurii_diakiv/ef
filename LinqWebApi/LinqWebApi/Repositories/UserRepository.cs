﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using LinqWebApi.Models;
using DataAccess.Entities;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace LinqWebApi.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly ProjectDbContext dbContext;

        public UserRepository(ProjectDbContext projectDbContext)
        {
            dbContext = projectDbContext;
        }

        public IEnumerable<User> GetItems()
        {
            return dbContext.Set<User>().AsEnumerable();
        }

        public User GetItem(int id)
        {
            return dbContext.Set<User>().Find(id);
        }

        public void Create(User p)
        {
            dbContext.Set<User>().Add(p);
            dbContext.SaveChanges();
        }

        public void Update(User p)
        {
            dbContext.Entry(p).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var p = dbContext.Set<User>().Find(id);
            dbContext.Set<User>().Remove(p);
            dbContext.SaveChanges();
        }



        //public LinqWebApi.Data data = new LinqWebApi.Data();
        //List<User> users = new List<User>();

        //public UserRepository()
        //{
        //    users = data.GetUsers().ToList();
        //}
        //public List<User> GetItemList()
        //{
        //    return users;
        //}

        //public User GetItem(int id)
        //{
        //    return users.Find(u => u.Id == id);
        //}

        //public void Create(User u)
        //{
        //    users.Add(u);
        //}

        //public void Update(int id, User u)
        //{
        //    int i = users.FindIndex(us => us.Id == id);
        //    users[i] = u;
        //}

        //public void Delete(int id)
        //{
        //    User u = users.Find(us => us.Id == id);
        //    if (u != null)
        //    {
        //        users.Remove(u);
        //    }
        //}
    }
}

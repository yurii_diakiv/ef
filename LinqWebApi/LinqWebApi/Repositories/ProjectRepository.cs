﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using LinqWebApi.Models;
using DataAccess.Entities;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace LinqWebApi.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly ProjectDbContext dbContext;

        public ProjectRepository(ProjectDbContext projectDbContext)
        {
            dbContext = projectDbContext;
        }

        public IEnumerable<Project> GetItems()
        {
            return dbContext.Set<Project>().AsEnumerable();
        }

        public Project GetItem(int id)
        {
            return dbContext.Set<Project>().Find(id);
        }

        public void Create(Project p)
        {
            dbContext.Set<Project>().Add(p);
            dbContext.SaveChanges();
        }

        public void Update(Project p)
        {
            dbContext.Entry(p).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var p = dbContext.Set<Project>().Find(id);
            dbContext.Set<Project>().Remove(p);
            dbContext.SaveChanges();
        }


        //public LinqWebApi.Data data = new LinqWebApi.Data();
        //List<Project> projects;
        //public ProjectRepository()
        //{
        //    projects = data.GetProjects().ToList();
        //}
        //public IEnumerable<Project> GetItems()
        //{
        //    return projects;
        //}

        //public Project GetItem(int id)
        //{
        //    return projects.Find(p => p.Id == id);
        //}

        //public void Create(Project p)
        //{
        //    projects.Add(p);
        //}

        //public void Update(/*int id, */Project p)
        //{
        //    //int i = projects.FindIndex(pr => pr.Id == id);
        //    //projects[i] = p;
        //}

        //public void Delete(int id)
        //{
        //    projects.Remove(projects.Find(p => p.Id == id));
        //    //Project p = projects.Find(pr => pr.Id == id);
        //    //if(p != null)
        //    //{
        //    //    projects.Remove(p);
        //    //}
        //}
    }
}

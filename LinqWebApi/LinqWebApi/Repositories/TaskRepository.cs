﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using DataAccess.Entities;
using DataAccess;
using Microsoft.EntityFrameworkCore;

namespace LinqWebApi.Repositories
{
    public class TaskRepository : IRepository<DataAccess.Entities.Task>
    {
        private readonly ProjectDbContext dbContext;

        public TaskRepository(ProjectDbContext projectDbContext)
        {
            dbContext = projectDbContext;
        }

        public IEnumerable<DataAccess.Entities.Task> GetItems()
        {
            return dbContext.Set<DataAccess.Entities.Task>().AsEnumerable();
        }

        public DataAccess.Entities.Task GetItem(int id)
        {
            return dbContext.Set<DataAccess.Entities.Task>().Find(id);
        }

        public void Create(DataAccess.Entities.Task p)
        {
            dbContext.Set<DataAccess.Entities.Task>().Add(p);
            dbContext.SaveChanges();
        }

        public void Update(DataAccess.Entities.Task p)
        {
            dbContext.Entry(p).State = EntityState.Modified;
            dbContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var p = dbContext.Set<DataAccess.Entities.Task>().Find(id);
            dbContext.Set<DataAccess.Entities.Task>().Remove(p);
            dbContext.SaveChanges();
        }


        //public LinqWebApi.Data data = new LinqWebApi.Data();
        //List<Models.Task> tasks;

        //public TaskRepository()
        //{
        //    tasks = data.GetTasks().ToList();
        //}
        //public List<Models.Task> GetItems()
        //{
        //    return tasks;
        //}

        //public Models.Task GetItem(int id)
        //{
        //    return tasks.Find(p => p.Id == id);
        //}

        //public void Create(Models.Task t)
        //{
        //    tasks.Add(t);
        //}

        //public void Update(int id, Models.Task t)
        //{
        //    int i = tasks.FindIndex(ta => ta.Id == id);
        //    tasks[i] = t;
        //}

        //public void Delete(int id)
        //{
        //    Models.Task t = tasks.Find(ta => ta.Id == id);
        //    if (t != null)
        //    {
        //        tasks.Remove(t);
        //    }
        //}
    }
}

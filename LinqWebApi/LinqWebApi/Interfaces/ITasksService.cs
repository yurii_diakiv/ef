﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinqWebApi.Interfaces
{
    interface ITasksService
    {
        IEnumerable<DataAccess.Entities.Task> GetTasks();
        DataAccess.Entities.Task GetTask(int id);
    }
}

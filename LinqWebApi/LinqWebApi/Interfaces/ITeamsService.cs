﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using LinqWebApi.Models;
using DataAccess.Entities;

namespace LinqWebApi.Interfaces
{
    interface ITeamsService
    {
        IEnumerable<Team> GetTeams();
        Team GetTeam(int id);
    }
}

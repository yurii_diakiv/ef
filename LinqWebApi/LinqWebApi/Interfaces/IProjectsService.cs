﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//using LinqWebApi.Models;
using DataAccess.Entities;

namespace LinqWebApi.Interfaces
{
    interface IProjectsService
    {
        IEnumerable<Project> GetProjects();
        Project GetProject(int id);
    }
}

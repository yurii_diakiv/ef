﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LinqWebApi.Services;
using System.IO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace LinqWebApi.Controllers
{
    [Route("api/[controller]")]
    public class MessageController : Controller
    {
        private readonly QueueService queueService;

        public MessageController(QueueService qService)
        {
            queueService = qService;
        }


        // GET: api/<controller>
        [HttpGet]
        public ActionResult<string> Get()
        {
            return "MessageController))";
        }

        [Route("~/api/message/alltext")]
        [HttpGet]
        public ActionResult<string> GetText()
        {
            string[] lines = System.IO.File.ReadAllLines(/*"../../../../Output.txt"*/$"C:/Навчання/Binary/HomeworksJuly/(3)ServiceCommunication/Output.txt");
            lines.Reverse();
            string text = "";
            foreach(var i in lines)
            {
                text += i;
                text += "\n";
            }
            return text;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        //public void Post([FromBody]string inputMessage)
        //{
        //    //queueService.PostValue(inputMessage);
        //}
        public ActionResult<bool> Post([FromBody]string inputMessage)
        {
            return new ActionResult<bool>(queueService.PostValue(inputMessage));
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

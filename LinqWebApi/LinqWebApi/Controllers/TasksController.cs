﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
//using LinqWebApi.Models;
using LinqWebApi.Services;
//using DataAccess.Entities;


namespace LinqWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TasksController : ControllerBase
    {
        private TasksService tasksService;

        public TasksController(TasksService service)
        {
            tasksService = service;
        }

        [HttpGet]
        public IEnumerable<DataAccess.Entities.Task> Get()
        {
            return tasksService.GetTasks();
        }

        [HttpGet("{id}")]
        public DataAccess.Entities.Task Get(int id)
        {
            return tasksService.GetTask(id);
        }

        [HttpPost]
        public void Post([FromBody] DataAccess.Entities.Task task)
        {
            tasksService.Create(task);
        }

        [HttpPut("{id}")]
        public void Put(/*int id, */[FromBody] DataAccess.Entities.Task task)
        {
            tasksService.Update(/*id, */task);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            tasksService.Delete(id);
        }
    }
}

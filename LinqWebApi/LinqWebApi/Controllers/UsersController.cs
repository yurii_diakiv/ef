﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LinqWebApi.Services;
//using LinqWebApi.Models;
using DataAccess.Entities;

namespace LinqWebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private UsersService usersService;

        public UsersController(UsersService service)
        {
            usersService = service;
        }

        [HttpGet]
        public IEnumerable<User> Get()
        {
            return usersService.GetUsers();
        }

        [HttpGet("{id}")]
        public User Get(int id)
        {
            return usersService.GetUser(id);
        }

        [HttpPost]
        public void Post([FromBody] User user)
        {
            usersService.Create(user);
        }

        [HttpPut("{id}")]
        public void Put(/*int id, */[FromBody] User user)
        {
            usersService.Update(/*id, */user);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            usersService.Delete(id);
        }
    }
}

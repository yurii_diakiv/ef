﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataAccess.Entities
{
    public class Project
    {
        public int Id { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int? AuthorId { get; set; }
        public int? TeamId { get; set; }

        
        public Team Team { get; set; }
        public User User { get; set; }
        public List<Task> Tasks { get; set; }

        public Project(int id, string projectName, string projectDescription, DateTime createdAt, DateTime deadline, int? authorId, int? teamId)
        {
            Id = id;
            ProjectName = projectName;
            ProjectDescription = projectDescription;
            CreatedAt = createdAt;
            Deadline = deadline;
            AuthorId = authorId;
            TeamId = teamId;
        }
    }
}

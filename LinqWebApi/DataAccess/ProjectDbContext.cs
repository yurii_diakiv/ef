﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using DataAccess.Entities;

namespace DataAccess
{
    public class ProjectDbContext : DbContext
    {
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options)
            :base(options) { }

        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskState> TaskStates { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                .HasOne<Project>(p => p.Project)
                .WithOne(u => u.User)
                .HasForeignKey<Project>(p => p.AuthorId);

            modelBuilder.Entity<Project>()
                .HasOne<User>(p => p.User)
                .WithOne(u => u.Project)
                .HasForeignKey<Project>(p => p.AuthorId);

            modelBuilder.Entity<Project>()
                .HasOne<Team>(p => p.Team)
                .WithOne(u => u.Project)
                .HasForeignKey<Project>(p => p.TeamId);

            modelBuilder.Entity<Task>()
                .HasOne<Project>(t => t.Project)
                .WithMany(t => t.Tasks)
                .HasForeignKey(t => t.ProjectId);

            modelBuilder.Entity<Task>()
                .HasOne<User>(t => t.User)
                .WithOne(u => u.Task)
                .HasForeignKey<Task>(t => t.PerformerId);

            modelBuilder.Entity<User>()
                .HasOne<Team>(u => u.Team)
                .WithMany(t => t.Users)
                .HasForeignKey(u => u.TeamId);

            DataAccess.Data data = new DataAccess.Data();
            List<Team> teams = data.GetTeams();

            modelBuilder.Entity<Team>().HasData(teams);
        }
    }
}

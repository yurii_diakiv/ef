﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataAccess.Migrations
{
    public partial class AddSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "labore" },
                    { 2, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "reprehenderit" },
                    { 3, new DateTime(2019, 6, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "qui" },
                    { 4, new DateTime(2019, 6, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "repudiandae" },
                    { 5, new DateTime(2019, 6, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "nobis" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
